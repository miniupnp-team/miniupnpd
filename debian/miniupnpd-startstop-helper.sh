#!/bin/sh -e

# This script might be called by sysv init script with env set correctly, but
# not if called by systemd service unit.

[ -z "${CONFFILE}" ] && CONFFILE=/etc/miniupnpd/miniupnpd.conf
[ -z "${DEFAULT}" ] && DEFAULT=/etc/default/miniupnpd
[ -z "${SCRIPT_DIR}" ] && SCRIPT_DIR=/etc/miniupnpd

[ -f "${DEFAULT}" ] && . "${DEFAULT}"

read_config () {
	sed -rn '
s|^\s*'"${2}"'='\''([^'\'']+)'\''\s*$|\1|g
t hold
s|^\s*'"${2}"'='\"'([^'\"']+)'\"'\s*$|\1|g
t hold
s|^\s*'"${2}"'=(\S+)\s*$|\1|
t hold
b
: hold
p
	' "${1}"
}

check_miniupnpd () {
	[ -n "${MiniUPnPd_NO_CHECK_BACKEND}" ] || \
		[ -n "${MiniUPnPd_PRESTART_COMMAND}" ] || \
		[ -n "${MiniUPnPd_POSTSTOP_COMMAND}" ] && return 0
	# detect iptables, since `iptables` may not be present if only nftables installed
	if update-alternatives --query iptables | grep -F Value | grep -qF legacy ; then
		system_backend=iptables
	else
		system_backend=nftables
	fi
	if { [ -f "${SCRIPT_DIR}/iptables_init.sh" ] && [ "${system_backend}" = nftables ]; } || \
			{ [ -f "${SCRIPT_DIR}/nft_init.sh" ] && [ "${system_backend}" = iptables ]; } ; then
		echo "WARNING: Your miniupnpd binary seems to mismatch your *tables backend; refused to start!"
		echo "WARNING: You should install 'miniupnpd-${system_backend}' instead"
		echo "WARNING: Read /usr/share/doc/miniupnpd/NEWS.Debian.gz for more information"
		return 1
	fi
}

get_ext_ifname () {
	if [ -f "${CONFFILE}" ] ; then
		ext_ifname=$(read_config "${CONFFILE}" ext_ifname)
		ext_ifname6=$(read_config "${CONFFILE}" ext_ifname6)
		[ -z "${ext_ifname6}" ] && ext_ifname6="${ext_ifname}"
	fi
	if [ -z "${ext_ifname}" ] ; then
		echo "Warning: no interface defined"
		return 1
	fi
}

case "${1}" in
	start)
		if [ -n "${MiniUPnPd_PRESTART_COMMAND}" ] ; then
			${MiniUPnPd_PRESTART_COMMAND}
			exit $?
		fi
		check_miniupnpd || exit 2
		if [ "${system_backend}" = nftables ] ; then
			"${SCRIPT_DIR}/nft_init.sh"
		elif get_ext_ifname ; then
			"${SCRIPT_DIR}/iptables_init.sh" -i "${ext_ifname}"
			if [ "${MiniUPnPd_ip6tables_enable}" = 1 ] ; then
				"${SCRIPT_DIR}/ip6tables_init.sh" -i "${ext_ifname6}"
			fi
		fi
		;;

	stop)
		if [ -n "${MiniUPnPd_POSTSTOP_COMMAND}" ] ; then
			${MiniUPnPd_POSTSTOP_COMMAND}
			exit $?
		fi
		check_miniupnpd || exit 2
		if [ "${system_backend}" = nftables ] ; then
			"${SCRIPT_DIR}/nft_removeall.sh"
		elif get_ext_ifname ; then
			"${SCRIPT_DIR}/iptables_removeall.sh" -i "${ext_ifname}"
			if [ "${MiniUPnPd_ip6tables_enable}" = 1 ] ; then
				"${SCRIPT_DIR}/ip6tables_removeall.sh" -i "${ext_ifname6}"
			fi
		fi
		;;

	*)
		echo "Usage: ${0} {start|stop}"
		exit 1
		;;
esac

exit 0
